package com.shadow.coder.moneymanage.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.shadow.coder.moneymanage.PrefManager
import com.shadow.coder.moneymanage.R
import com.shadow.coder.moneymanage.activity.Login
import com.shadow.coder.moneymanage.db.DatabaseData
import com.shadow.coder.moneymanage.db.DatabaseQuery
import java.util.*

class Login : AppCompatActivity() {
    var signUp: LinearLayout? = null
    var login: TextView? = null
    var databaseQuery: DatabaseQuery? = null
    var userPhone: EditText? = null
    var userPassword: EditText? = null
    var forgetPassword: TextView? = null
    var databaseData: ArrayList<DatabaseData?>? = null
    var prefManager: PrefManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        databaseQuery = DatabaseQuery(this)
        databaseData = ArrayList()
        prefManager = PrefManager(this)
        setProperty()
        setListener()
    }

    private fun setProperty() {
        signUp = findViewById<LinearLayout?>(R.id.sign_up)
        login = findViewById<TextView?>(R.id.login)
        userPhone = findViewById<EditText?>(R.id.user_phone)
        userPassword = findViewById<EditText?>(R.id.password)
        forgetPassword = findViewById<TextView?>(R.id.forget_password)
    }

    private fun setListener() {
        signUp!!.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@Login, Registration::class.java))
            finish()
        })
        login!!.setOnClickListener(View.OnClickListener {
            if (userPhone!!.getText().toString().equals("", ignoreCase = true)) {
                userPhone!!.setError("Please enter your phone no")
            } else if (userPassword!!.getText().toString().equals("", ignoreCase = true)) {
                userPassword!!.setError("Please enter your password")
            } else {
                databaseQuery!!.open()
                if (databaseQuery!!.CHECK_LOGIN(userPhone!!.getText().toString(), userPassword!!.getText().toString())) {
                    databaseData = databaseQuery!!.GET_USER_DETAILS(userPhone!!.getText().toString())
                    prefManager!!.setUserId(databaseData!!.get(0)!!.getUSER_ID())
                    prefManager!!.setLoginStatus(true)
                    startActivity(Intent(this@Login, Home::class.java))
                    finish()
                } else {
                    Toast.makeText(this@Login, "Wrong phone or password", Toast.LENGTH_SHORT).show()
                }
                databaseQuery!!.close()
            }
        })
    }
}