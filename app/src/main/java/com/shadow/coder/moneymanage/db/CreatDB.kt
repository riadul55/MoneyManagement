package com.shadow.coder.moneymanage.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class CreatDB(context: Context?) : SQLiteOpenHelper(context, DATABASE_NAME, null, DBVERSION) {
    override fun onCreate(db: SQLiteDatabase) {
        var cv = ContentValues()
        db.execSQL(CREATE_USER_ACCOUNT)
        db.execSQL(CREATE_INCOME_DATABASE)
        db.execSQL(CREATE_SPEND_DATABASE)
        db.execSQL(CREATE_SPEND_CATEGORY)
        db.execSQL(CREATE_DEBIT_DATABASE)
        db.execSQL(CREATE_CREDIT_DATABASE)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Food & Drink")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Shoping")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Transport")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Home")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Bills & Fees")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Entertainment")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Car")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Travel")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Faily & Personal")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Health Care")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Education")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Groceries")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Gifts")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Sports & Hobbies")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Beauty")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Food")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Work")
        db.insert(TABLE_SPEND_CATEGORY, null, cv)
        cv = ContentValues()
        cv.put(CATEGORY_NAME, "Add a category")
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        when (newVersion) {
        }
    }

    companion object {
        private const val DBVERSION = 1
        private val DATABASE_NAME: String? = "money_management"
        val USER_ID: String? = "user_id"
        val USER_NAME: String? = "user_name"
        val USER_COMPANY: String? = "user_company"
        val USER_DESIGNATION: String? = "user_designation"
        val USER_PHONE: String? = "user_phone"
        val USER_EMAIL: String? = "user_email"
        val USER_PASSWORD: String? = "user_password"
        val INCOME_CATEGORY: String? = "income_category"
        val INCOME: String? = "income"
        val DATE: String? = "date"
        val SPEND_CATEGORY: String? = "spend_category"
        val SPEND_DESCRIPTION: String? = "spend_description"
        val SPEND: String? = "spend"
        val ID: String? = "id"
        val CATEGORY_NAME: String? = "category_name"
        val LOAN_ID: String? = "loan_id"
        val HOLDER_NAME: String? = "holder_name"
        val DEBIT: String? = "debit"
        val CREDIT: String? = "credit"
        val TABLE_USER_ACCOUNT: String? = "t_user_account"
        val TABLE_INCOME_DATABASE: String? = "t_income_database"
        val TABLE_SPEND_DATABASE: String? = "t_spend_database"
        val TABLE_SPEND_CATEGORY: String? = "t_spend_category"
        val TABLE_DEBIT_DATABASE: String? = "t_debit_database"
        val TABLE_CREDIT_DATABASE: String? = "t_credit_database"
        private val CREATE_USER_ACCOUNT: String? = "CREATE TABLE IF NOT EXISTS $TABLE_USER_ACCOUNT ( $USER_ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, $USER_NAME TEXT, $USER_COMPANY TEXT, $USER_DESIGNATION TEXT, $USER_PHONE TEXT, $USER_EMAIL TEXT,$USER_PASSWORD TEXT );"
        private val CREATE_INCOME_DATABASE: String? = "CREATE TABLE IF NOT EXISTS $TABLE_INCOME_DATABASE ( $USER_ID INTEGER NOT NULL, $INCOME_CATEGORY TEXT, $INCOME TEXT );"
        private val CREATE_SPEND_DATABASE: String? = "CREATE TABLE IF NOT EXISTS $TABLE_SPEND_DATABASE ( $USER_ID INTEGER NOT NULL, $SPEND_CATEGORY TEXT, $SPEND_DESCRIPTION TEXT, $SPEND TEXT, $DATE TEXT );"
        private val CREATE_SPEND_CATEGORY: String? = "CREATE TABLE IF NOT EXISTS $TABLE_SPEND_CATEGORY ( $ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, $CATEGORY_NAME TEXT );"
        private val CREATE_DEBIT_DATABASE: String? = "CREATE TABLE IF NOT EXISTS $TABLE_DEBIT_DATABASE ( $USER_ID INTEGER NOT NULL, $LOAN_ID INTEGER, $HOLDER_NAME TEXT, $DEBIT TEXT, $CREDIT, $DATE TEXT );"
        private val CREATE_CREDIT_DATABASE: String? = "CREATE TABLE IF NOT EXISTS $TABLE_CREDIT_DATABASE ( $USER_ID INTEGER NOT NULL, $LOAN_ID INTEGER, $HOLDER_NAME TEXT, $DEBIT TEXT, $CREDIT, $DATE TEXT );"
    }
}