package com.shadow.coder.moneymanage.text_view

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView

class RighteousRegular : AppCompatTextView {
    constructor(context: Context?) : super(context) {}
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {}
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {}

    fun init() {
        val tf = Typeface.createFromAsset(context.assets, "fonts/Righteous-Regular.ttf")
        typeface = tf
    }
}