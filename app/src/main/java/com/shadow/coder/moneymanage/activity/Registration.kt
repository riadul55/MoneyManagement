package com.shadow.coder.moneymanage.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.shadow.coder.moneymanage.PrefManager
import com.shadow.coder.moneymanage.R
import com.shadow.coder.moneymanage.activity.Registration
import com.shadow.coder.moneymanage.db.DatabaseQuery
import java.util.*

class Registration : AppCompatActivity() {
    var below: LinearLayout? = null
    var registration: TextView? = null
    var userName: EditText? = null
    var userEmail: EditText? = null
    var userPhone: EditText? = null
    var password: EditText? = null
    var re_password: EditText? = null
    var databaseQuery: DatabaseQuery? = null
    var prefManager: PrefManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        databaseQuery = DatabaseQuery(this)
        prefManager = PrefManager(this)
        setProperty()
        setListener()
        setValue()
    }

    private fun setProperty() {
        below = findViewById<LinearLayout?>(R.id.below)
        registration = findViewById<TextView?>(R.id.registration)
        userName = findViewById<EditText?>(R.id.user_name)
        userEmail = findViewById<EditText?>(R.id.user_email)
        userPhone = findViewById<EditText?>(R.id.user_phone)
        password = findViewById<EditText?>(R.id.password)
        re_password = findViewById<EditText?>(R.id.c_pass)
    }

    private fun setListener() {
        below!!.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@Registration, Login::class.java))
            finish()
        })
        registration!!.setOnClickListener(View.OnClickListener {
            if (userName!!.getText().toString().equals("", ignoreCase = true)) {
                userName!!.setError("Need an user name")
            } else if (userPhone!!.getText().toString().length < 11) {
                userPhone!!.setError("Please enter right phone no")
            } else if (userEmail!!.getText().toString().equals("", ignoreCase = true)) {
                userEmail!!.setError("Need email address")
            } else if (password!!.getText().toString().length < 6) {
                password!!.setError("Need at least 6 character")
            } else if (password!!.getText().toString() != re_password!!.getText().toString()) {
                re_password!!.setError("Password are not matched")
            } else {
                val ran = Random()
                val `val` = ran.nextInt(99999)
                databaseQuery!!.open()
                databaseQuery!!.INSERT_USER(`val`.toString() + "",
                        userName!!.getText().toString(),
                        userPhone!!.getText().toString(),
                        userEmail!!.getText().toString(),
                        password!!.getText().toString())
                databaseQuery!!.close()
                prefManager!!.setUserId(`val`.toString() + "")
                prefManager!!.setLoginStatus(true)
                Toast.makeText(this@Registration, "Registration successfully", Toast.LENGTH_SHORT).show()
                startActivity(Intent(this@Registration, Home::class.java))
                finish()
            }
        })
    }

    fun setValue() {}
}