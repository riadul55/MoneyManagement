package com.shadow.coder.moneymanage.db

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import java.util.*

class DatabaseQuery(context: Context?) {
    lateinit var sqLiteDatabase:SQLiteDatabase
    var creatDB = CreatDB(context)
    var USER_ID: String? = "user_id"
    var USER_NAME: String? = "user_name"
    var USER_PHONE: String? = "user_phone"
    var USER_COMPANY: String? = "user_company"
    var USER_DESIGNATION: String? = "user_designation"
    var USER_EMAIL: String? = "user_email"
    var USER_PASSWORD: String? = "user_password"
    var INCOME_CATEGORY: String? = "income_category"
    var INCOME: String? = "income"
    var DATE: String? = "date"
    var SPEND_CATEGORY: String? = "spend_category"
    var SPEND_DESCRIPTION: String? = "spend_description"
    var SPEND: String? = "spend"
    var ID: String? = "id"
    var CATEGORY_NAME: String? = "category_name"
    var LOAN_ID: String? = "loan_id"
    var HOLDER_NAME: String? = "holder_name"
    var DEBIT: String? = "debit"
    var CREDIT: String? = "credit"
    var TABLE_USER_ACCOUNT: String? = "t_user_account"
    var TABLE_INCOME_DATABASE: String? = "t_income_database"
    var TABLE_SPEND_DATABASE: String? = "t_spend_database"
    var TABLE_SPEND_CATEGORY: String? = "t_spend_category"
    var TABLE_DEBIT_DATABASE: String? = "t_debit_database"
    var TABLE_CREDIT_DATABASE: String? = "t_credit_database"
    fun open() {
        sqLiteDatabase = creatDB.getWritableDatabase()
    }

    fun close() {
        sqLiteDatabase.close()
    }

    fun INSERT_USER(user_id: String?, user_name: String?, user_phone: String?, user_email: String?, user_password: String?) {
        val cv = ContentValues()
        cv.put(USER_ID, user_id)
        cv.put(USER_NAME, user_name)
        cv.put(USER_PHONE, user_phone)
        cv.put(USER_EMAIL, user_email)
        cv.put(USER_PASSWORD, user_password)
        sqLiteDatabase.insert(TABLE_USER_ACCOUNT, null, cv)
    }

    fun UPDATE_USER(user_id: String?, name: String?, email: String?, compnay_name: String?, designation: String?) {
        val cv = ContentValues()
        cv.put(USER_NAME, name)
        cv.put(USER_EMAIL, email)
        cv.put(USER_COMPANY, compnay_name)
        cv.put(USER_DESIGNATION, designation)
        sqLiteDatabase.update(TABLE_USER_ACCOUNT, cv, " id=$user_id", null)
    }

    fun CHECK_LOGIN(user_phone: String?, user_password: String?): Boolean {
        var login_status = false
        val cursor = sqLiteDatabase.rawQuery("select user_id from $TABLE_USER_ACCOUNT where $USER_PHONE like '$user_phone' and $USER_PASSWORD like '$user_password'", null)
        if (cursor.count > 0) {
            login_status = true
        }
        cursor.close()
        return login_status
    }

    fun GET_USER_DETAILS(user_phone: String?): ArrayList<DatabaseData?>? {
        val user_details = ArrayList<DatabaseData?>()
        val cursor = sqLiteDatabase.rawQuery("select * from $TABLE_USER_ACCOUNT where $USER_PHONE = '$user_phone'", null)
        if (cursor.count > 0) {
            cursor.moveToFirst()
            for (i in 0 until cursor.count) {
                val user_id = cursor.getString(cursor.getColumnIndex(USER_ID))
                val user_name = cursor.getString(cursor.getColumnIndex(USER_NAME))
                val u_phone = cursor.getString(cursor.getColumnIndex(USER_PHONE))
                val user_mail = cursor.getString(cursor.getColumnIndex(USER_EMAIL))
                val user_company = cursor.getString(cursor.getColumnIndex(USER_COMPANY))
                val user_designation = cursor.getString(cursor.getColumnIndex(USER_DESIGNATION))
                val dbd = DatabaseData(user_id, user_name, u_phone, user_mail, user_company, user_designation)
                user_details.add(dbd)
                cursor.moveToNext()
            }
        }
        cursor.close()
        return user_details
    }

    fun addIncomeSource(user_id: String?, category: String?, income: String?): Long {
        val cv = ContentValues()
        cv.put(USER_ID, user_id)
        cv.put(INCOME_CATEGORY, income)
        cv.put(INCOME, income)
        return sqLiteDatabase.insert(TABLE_INCOME_DATABASE, null, cv)
    }

    fun getTotalIncome(user_id: String?, date: String?): Int { //get total income by user id and year with month;
        var amount = 0
        var q = "select $INCOME from $TABLE_INCOME_DATABASE where $USER_ID = '$user_id'"
        if (!date.equals("", ignoreCase = true)) {
            q += " and $DATE = '$date%'"
        }
        val cursor = sqLiteDatabase.rawQuery(q, null)
        if (cursor.count > 0) {
            cursor.moveToFirst()
            for (i in 0 until cursor.count) {
                amount += cursor.getString(cursor.getColumnIndex(INCOME)).toInt()
                cursor.moveToNext()
            }
        }
        cursor.close()
        return amount
    }

    fun getIncomeCategory(user_id: String?): ArrayList<DatabaseData?>? { // get all income category by user wise
        val incomeCategoryList = ArrayList<DatabaseData?>()
        val cursor = sqLiteDatabase.rawQuery("select * from $TABLE_INCOME_DATABASE where $USER_ID = '$user_id'", null)
        if (cursor.count > 0) {
            cursor.moveToFirst()
            for (i in 0 until cursor.count) {
                val incomeCategory = cursor.getString(cursor.getColumnIndex(INCOME_CATEGORY))
                val income = cursor.getString(cursor.getColumnIndex(INCOME))
                val databaseData = DatabaseData(user_id, incomeCategory, income)
                incomeCategoryList.add(databaseData)
                cursor.moveToNext()
            }
        }
        cursor.close()
        return incomeCategoryList
    }

    fun getTodaySpend(user_id: String?, date: String?): ArrayList<DatabaseData?>? { //get today spend
        val spendList = ArrayList<DatabaseData?>()
        val cursor = sqLiteDatabase.rawQuery("select $TABLE_SPEND_DATABASE.*, $TABLE_SPEND_CATEGORY.$CATEGORY_NAME from $TABLE_SPEND_DATABASE inner join $TABLE_SPEND_CATEGORY on $TABLE_SPEND_DATABASE.$SPEND_CATEGORY=$TABLE_SPEND_CATEGORY.$ID where $USER_ID = '$user_id' and $DATE='$date'", null)
        if (cursor.count > 0) {
            cursor.moveToFirst()
            for (i in 0 until cursor.count) {
                val spendCategory = cursor.getString(cursor.getColumnIndex(CATEGORY_NAME))
                val des = cursor.getString(cursor.getColumnIndex(SPEND_DESCRIPTION))
                val spend = cursor.getString(cursor.getColumnIndex(SPEND))
                val databaseData = DatabaseData(user_id, spendCategory, des, spend, date)
                spendList.add(databaseData)
                cursor.moveToNext()
            }
        }
        cursor.close()
        return spendList
    }

}