package com.shadow.coder.moneymanage

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor

class PrefManager(context: Context?) {
    var preferences: SharedPreferences=context!!.getSharedPreferences(prefName, Context.MODE_PRIVATE)
    var editor: Editor = preferences.edit()
    fun setLoginStatus(status: Boolean) {
        editor.putBoolean(LOGIN_STATUS, status)
        editor.commit()
    }

    fun isLoginStatus(): Boolean {
        return preferences.getBoolean(LOGIN_STATUS, false)
    }

    fun setUserId(userId: String?) {
        editor.putString(USER_ID, userId)
        editor.commit()
    }

    fun getUserId(): String? {
        return preferences.getString(USER_ID, "")
    }

    companion object {
        private val prefName: String? = "Money_Management"
        private val USER_ID: String? = "user_id"
        private val LOGIN_STATUS: String? = "login_status"
    }

}