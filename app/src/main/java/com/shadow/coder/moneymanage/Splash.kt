package com.shadow.coder.moneymanage

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.shadow.coder.moneymanage.activity.Home
import com.shadow.coder.moneymanage.activity.Login

class Splash : AppCompatActivity() {
    companion object {
        // Used to load the 'native-lib' library on application startup.
        init {
            System.loadLibrary("native-lib")
        }
    }

    var prefManager: PrefManager? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        prefManager = PrefManager(this)
        splashHandler()
    }

    fun splashHandler() {
        Handler().postDelayed({
            if (prefManager!!.isLoginStatus()) {
                startActivity(Intent(this@Splash, Home::class.java))
                finish()
            } else {
                startActivity(Intent(this@Splash, Login::class.java))
                finish()
            }
        }, 2000)
    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String?
}