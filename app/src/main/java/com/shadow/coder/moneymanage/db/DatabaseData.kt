package com.shadow.coder.moneymanage.db

class DatabaseData {
    private var USER_ID: String? = ""
    private var USER_NAME: String? = ""
    private var USER_PHONE: String? = ""
    private var USER_COMPANY: String? = ""
    private var USER_DESIGNATION: String? = ""
    private var USER_EMAIL: String? = ""
    private val USER_PASSWORD: String? = ""
    private var INCOME_CATEGORY: String? = ""
    private var INCOME: String? = ""
    private var DATE: String? = ""
    private val SPEND_CATEGORY: String? = ""
    private var SPEND_DESCRIPTION: String? = ""
    private var SPEND: String? = ""
    private val ID: String? = ""
    private var CATEGORY_NAME: String? = ""
    private val LOAN_ID: String? = ""
    private val HOLDER_NAME: String? = ""
    private val DEBIT: String? = ""
    private val CREDIT: String? = ""

    constructor(user_id: String?, user_name: String?, user_phone: String?, user_email: String?, user_company: String?, user_designation: String?) {
        USER_ID = user_id
        USER_NAME = user_name
        USER_PHONE = user_phone
        USER_EMAIL = user_email
        USER_COMPANY = user_company
        USER_DESIGNATION = user_designation
    }

    constructor(user_id: String?, income_category: String?, income: String?) {
        USER_ID = user_id
        INCOME_CATEGORY = income_category
        INCOME = income
    }

    constructor(user_id: String?, categoryName: String?, description: String?, spend: String?, date: String?) {
        USER_ID = user_id
        CATEGORY_NAME = categoryName
        SPEND_DESCRIPTION = description
        SPEND = spend
        DATE = date
    }

    fun getUSER_ID(): String? {
        return USER_ID
    }

    fun getUSER_NAME(): String? {
        return USER_NAME
    }

    fun getUSER_PHONE(): String? {
        return USER_PHONE
    }

    fun getUSER_COMPANY(): String? {
        return USER_COMPANY
    }

    fun getUSER_DESIGNATION(): String? {
        return USER_DESIGNATION
    }

    fun getUSER_EMAIL(): String? {
        return USER_EMAIL
    }

    fun getUSER_PASSWORD(): String? {
        return USER_PASSWORD
    }

    fun getINCOME_CATEGORY(): String? {
        return INCOME_CATEGORY
    }

    fun getINCOME(): String? {
        return INCOME
    }

    fun getDATE(): String? {
        return DATE
    }

    fun getSPEND_CATEGORY(): String? {
        return SPEND_CATEGORY
    }

    fun getSPEND_DESCRIPTION(): String? {
        return SPEND_DESCRIPTION
    }

    fun getSPEND(): String? {
        return SPEND
    }

    fun getID(): String? {
        return ID
    }

    fun getCATEGORY_NAME(): String? {
        return CATEGORY_NAME
    }

    fun getLOAN_ID(): String? {
        return LOAN_ID
    }

    fun getHOLDER_NAME(): String? {
        return HOLDER_NAME
    }

    fun getDEBIT(): String? {
        return DEBIT
    }

    fun getCREDIT(): String? {
        return CREDIT
    }
}