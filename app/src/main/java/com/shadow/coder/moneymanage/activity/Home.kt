package com.shadow.coder.moneymanage.activity

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.navigation.NavigationView
import com.shadow.coder.moneymanage.R
import com.shadow.coder.moneymanage.activity.Home

class Home : AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener {
    var dialog: AlertDialog? = null
    var today: TextView? = null
    var yesterday: TextView? = null
    var monthly: TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val toolbar = findViewById<View?>(R.id.toolbar) as Toolbar?
        setSupportActionBar(toolbar)
        setProperty()
        setListener()
        setValue()
        val fab = findViewById<View?>(R.id.fab) as FloatingActionButton?
        fab!!.setOnClickListener(View.OnClickListener { showSpendAddDialog() })
        val drawer = findViewById<View?>(R.id.drawer_layout) as DrawerLayout?
        val toggle = ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer!!.addDrawerListener(toggle)
        toggle.syncState()
        val navigationView = findViewById<View?>(R.id.nav_view) as NavigationView?
        navigationView!!.setItemIconTintList(null)
        navigationView!!.setNavigationItemSelectedListener(this)
    }

    fun setProperty() {
        today = findViewById<TextView?>(R.id.today)
        yesterday = findViewById<TextView?>(R.id.yesterday)
        monthly = findViewById<TextView?>(R.id.monthly)
    }

    fun setListener() {
        today!!.setOnClickListener(View.OnClickListener {
            today!!.setBackgroundResource(R.drawable.left_select)
            yesterday!!.setBackgroundResource(R.drawable.middle_deselect)
            monthly!!.setBackgroundResource(R.drawable.right_deselect)
            today!!.setTextColor(resources.getColor(R.color.colorPrimary))
            yesterday!!.setTextColor(resources.getColor(R.color.white))
            monthly!!.setTextColor(resources.getColor(R.color.white))
        })
        yesterday!!.setOnClickListener(View.OnClickListener {
            today!!.setBackgroundResource(R.drawable.left_deselect)
            yesterday!!.setBackgroundResource(R.drawable.middle_select)
            monthly!!.setBackgroundResource(R.drawable.right_deselect)
            today!!.setTextColor(resources.getColor(R.color.white))
            yesterday!!.setTextColor(resources.getColor(R.color.colorPrimary))
            monthly!!.setTextColor(resources.getColor(R.color.white))
        })
        monthly!!.setOnClickListener(View.OnClickListener {
            today!!.setBackgroundResource(R.drawable.left_deselect)
            yesterday!!.setBackgroundResource(R.drawable.middle_deselect)
            monthly!!.setBackgroundResource(R.drawable.right_select)
            today!!.setTextColor(resources.getColor(R.color.white))
            yesterday!!.setTextColor(resources.getColor(R.color.white))
            monthly!!.setTextColor(resources.getColor(R.color.colorPrimary))
        })
    }

    fun setValue() {}
    //=============spend add option
    fun showSpendAddDialog() {
        val inflater = layoutInflater
        val addLayout = inflater.inflate(R.layout.spend_add_layout, null)
        val alert = AlertDialog.Builder(this@Home)
        alert.setView(addLayout)
        val add = addLayout.findViewById<TextView?>(R.id.add)
        add!!.setOnClickListener(View.OnClickListener { })
        dialog = alert.create()
        dialog!!.getWindow().setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog!!.show()
    }

    override fun onBackPressed() {
        val drawer = findViewById<View?>(R.id.drawer_layout) as DrawerLayout?
        if (drawer!!.isDrawerOpen(GravityCompat.START)) {
            drawer!!.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onNavigationItemSelected(p0: MenuItem): Boolean {
        val id = p0.getItemId()
        if (id == R.id.edit_profile) {
            val intent = Intent(this@Home, MyProfile::class.java)
            startActivity(intent)
            finish()
            // Handle the camera action
        } else if (id == R.id.share) {
        } else if (id == R.id.feedback) {
        } else if (id == R.id.rate_us) {
        } else if (id == R.id.about) {
        } else if (id == R.id.setting) {
        }
        val drawer = findViewById<View?>(R.id.drawer_layout) as DrawerLayout?
        drawer!!.closeDrawer(GravityCompat.START)
        return true
    }
}